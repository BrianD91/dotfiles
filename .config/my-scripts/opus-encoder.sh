#!/usr/bin/bash

find ~/Programs/SMLoadr/DOWNLOADS -name '*.flac' -exec ffmpeg -i '{}' '{}.opus' ';'
find ~/Programs/SMLoadr/DOWNLOADS -name \*.opus -exec mv {} ~/Music/OpusEncoded \;
mv ~/Programs/SMLoadr/DOWNLOADS/*(DN) Music/ ;
